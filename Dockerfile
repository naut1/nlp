FROM anibali/pytorch:2.0.0-cuda11.8


USER root

WORKDIR /app

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
RUN apt-get update && apt-get install -y  git
RUN pip install tqdm

RUN pip install --no-cache-dir pandas torchtext spacy nltk

RUN pip install gdown

